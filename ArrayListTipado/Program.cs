﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTipado
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> dadosTipados = new List<double>();
            dadosTipados.Add(5.5);
            dadosTipados.Add(6.4);
            dadosTipados.Add(3.1);
            dadosTipados.Add(7.9);
            dadosTipados.Add(12.4);

            Console.WriteLine(dadosTipados.Average());
            Console.ReadKey();


            List<string> dadosTipados2 = new List<string>();
            dadosTipados2.Add("Juliana");
            dadosTipados2.Add("Bruna");
            dadosTipados2.Add("Natália");
            dadosTipados2.Add("Fernanda");
            dadosTipados2.Add("Marisa");
        }
    }
}
